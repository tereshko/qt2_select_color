#include "Squares.h"
#include <QPainter>
#include <QDebug>

Squares::Squares(){
	int countOfElements = m_columns*m_lines;
	
	m_qColorList = new QColor[countOfElements];
	for( int i = 0; i < countOfElements; i++){
		m_qColorList[i] = Qt::white;
	}
	m_colorFromButton = nullptr;
}

void Squares::paintEvent( QPaintEvent*){
	m_width = this->width();
	m_heigdht = this->height();

	m_squareWidth = m_width / m_columns;
	m_squareHeight = m_heigdht / m_lines;
	
	QPainter painter;
	painter.begin(this);
	painter.setRenderHint( QPainter::Antialiasing, true);

	int colorId = 0;
	for ( int i = 0; i < m_columns; i++) {
		for ( int j = 0; j < m_lines; j++) {
			QColor colorForPaint = m_qColorList[color];
			QBrush brushRect( colorForPaint, Qt::SolidPattern);
			painter.setBrush( brushRect);
			int x = m_squareWidth * i;
			int y = m_squareHeight*j;
			painter.drawRect( x, y, m_squareWidth, m_squareHeight);
			colorId++;
		}
	}
	painter.end();
}

void Squares::mousePressEvent(QMouseEvent *event){
	int pressX = event->x();
	int pressY = event->y();

	int modX = pressX / m_squareWidth;
	int modY = pressY / m_squareHeight;

	int minCoorX = modX * m_squareWidth;
	int minCoorY = modY * m_squareHeight;

	if(( pressX >= minCoorX) || ( pressX <= minCoorX+m_squareWidth)){
		if(( pressY >= minCoorY) || ( pressY <= minCoorY+m_squareHeight)){
			int numberOfElement = (m_lines*modX + modY);
			m_qColorList[ numberOfElement] = m_colorFromButton;
			this->repaint();
		}
	}
}

void Squares::setColor( QColor color){
	m_colorFromButton = color;
	this->repaint();
}

