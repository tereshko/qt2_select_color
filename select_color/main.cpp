#include <QtWidgets>
#include "MyWidget.h"
#include "Squares.h"

int main (int argc, char* argv[]){
	QApplication app(argc, argv);
	MyWidget myWidget;
	myWidget.resize(400,400);
	myWidget.show();
	return app.exec();
}