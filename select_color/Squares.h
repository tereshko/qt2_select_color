#ifndef SQUARES_H
#define SQUARES_H

#include <QWidget>
#include <QMouseEvent>

class Squares : public QWidget{
	
protected:
	void mousePressEvent( QMouseEvent* event);
	void paintEvent( QPaintEvent*);
public:
	Squares();
	void setColor( QColor color);

private:
	int m_width;
	int m_heigdht;

	int m_columns = 4;
	int m_lines = 7;
	QColor* m_qColorList;
	QColor m_colorFromButton;

	int m_squareWidth;
	int m_squareHeight;
};

#endif // SQUARES_H