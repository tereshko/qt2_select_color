#include "MyWidget.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QSignalMapper>
#include "Squares.h"

MyWidget::MyWidget(){

	
	//main Layoyt
	QHBoxLayout* mainLayout = new QHBoxLayout;

	QSignalMapper* mapper = new QSignalMapper(this);
	connect( mapper, SIGNAL( mapped( const QString)), this, SLOT( setColor( QString)));
	
	// Layout with buttons
	m_ColorRed = new QPushButton(this);
	m_ColorGreen = new QPushButton(this);
	m_colorBlue = new QPushButton(this);
	
	m_ColorRed->setText("Red");
	m_ColorGreen->setText("Green");
	m_colorBlue->setText("Blue");
	
	connect( m_ColorRed, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_ColorRed, "Qt::red");
	
	connect( m_ColorGreen, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_ColorGreen, "Qt::green");
	
	connect( m_colorBlue, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_colorBlue, "Qt::blue");
	
	QVBoxLayout* colorButtonLayout = new QVBoxLayout;
	colorButtonLayout->addWidget( m_ColorRed);
	colorButtonLayout->addWidget( m_ColorGreen);
	colorButtonLayout->addWidget( m_colorBlue);
	
	// Layout with squares
	QGridLayout* colorSquaresLayout = new QGridLayout;
	
	m_squa = new Squares();
	colorSquaresLayout->addWidget( m_squa);
	

	//setMainLayout
	mainLayout->addLayout( colorButtonLayout);
	mainLayout->addLayout( colorSquaresLayout);
	setLayout( mainLayout);
	
}

void MyWidget::setColor( QString color){
	Squares sQuares;
	QColor stringToColor;
	if( color == "Qt::blue"){
		stringToColor = Qt::blue;
	}
	if( color == "Qt::green"){
		stringToColor = Qt::green;
	}
	if( color == "Qt::red"){
		stringToColor = Qt::red;
	}
	
	
	sQuares.setColor( stringToColor);
	qDebug() << color;
}


