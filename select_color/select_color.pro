QT += widgets
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    MyWidget.cpp \
    Squares.cpp

INSTALLS += target

HEADERS += \
    MyWidget.h \
    Squares.h
