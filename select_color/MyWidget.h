#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QMouseEvent>
#include "Squares.h"

class MyWidget : public QWidget{
	Q_OBJECT
public:
	MyWidget();
	Squares* m_squa;
	
public slots:
	void setColor( QString colorString);
private:
	QPushButton* m_ColorRed;
	QPushButton* m_ColorGreen;
	QPushButton* m_colorBlue;
};

#endif // MYWIDGET_H